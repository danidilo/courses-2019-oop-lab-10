package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.stream.Collectors.*;

import java.util.Comparator;
import java.util.stream.*;;

/**
 *
 */
public final class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        var stream = songs.stream()
                                .map(i -> i.getSongName())
                                .sorted();
        return stream;
    }

    @Override
    public Stream<String> albumNames() {
        Supplier<String> s = (() -> "Nessun album");
        Stream<String> stream = songs.stream()
                                .map(i -> i.getAlbumName().orElse(s.get()));
        return stream;
    }

    @Override
    public Stream<String> albumInYear(final int year) {
        Stream<String> stream = albums.entrySet().stream()
                                .filter(i -> i.getValue() == year)
                                .map(i -> i.getKey());
        return stream;
    }

    @Override
    public int countSongs(final String albumName) {
        int res = 0;
        res = (int) songs.stream()
                .filter(i -> i.getAlbumName().equals(Optional.of(albumName)))
                .count();
        System.out.println(res);
        return res;
    }

    @Override
    public int countSongsInNoAlbum() {
        int numberSongs = (int) songs.stream()
                .filter(i -> i.getAlbumName().isEmpty())
                .count();
        return numberSongs;
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        OptionalDouble res = songs.stream()
                             .filter(i -> i.getAlbumName().equals(Optional.of(albumName)))
                             .mapToDouble(i -> i.duration)
                             .average();
        return res;
    }

    @Override
    public Optional<String> longestSong() {
        OptionalDouble longestDuration = songs.stream()
                               .mapToDouble(i -> i.getDuration())
                               .max();
        Optional<String> res = songs.stream()
                                .filter(i -> i.getDuration() == longestDuration.getAsDouble())
                                .map(i -> i.getSongName())
                                .findFirst();
        return res;
    }

    @Override
    public Optional<String> longestAlbum() {
        Map<String, Double> mappa = new HashMap<>();
        this.albums.forEach((i, k) -> {
            mappa.put(i, 0.0);
        });;
        songs.stream()
        .forEach(i -> {
            if (!i.getAlbumName().isEmpty()) {
                mappa.replace(i.getAlbumName().get(), mappa.get(i.getAlbumName().get()) + i.duration);
            }
        });
        Optional<String> res = Optional.of(mappa.entrySet().stream()
                                .max((i, k) -> {
                                    if (i.getValue() < k.getValue()) {
                                        return -1;
                                    }
                                    if (i.getValue() > k.getValue()) {
                                        return 1;
                                    }
                                    return 0;
                                })
                                .get().getKey());
        return res;
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
